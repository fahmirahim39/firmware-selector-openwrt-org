# 0.3.5 - 2021-08-22

* Don't offer snapshot builds of upcoming releases anymore
* Translation improvements
* Remove [*ASU*](https://github.com/aparcar/asu/) integration for now.
  * The feature wasn't fully integrated therefore it should be removed for now.

# 0.3.6 - 2021-08-29

* Rework of search function so matching of multiple strings now works! This
  comes in handy if one type for example *Foobar 200* but the device is
  actually called *Foo-Bar 200*. With the improved search function the device
  is still suggested.
